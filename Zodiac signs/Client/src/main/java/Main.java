import proto.Zodiac;
import proto.PersonServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 8999).usePlaintext().build();
        PersonServiceGrpc.PersonServiceBlockingStub personStub = PersonServiceGrpc.newBlockingStub(channel);

        boolean isRunning = true;
        while (isRunning)
        {
            Scanner scan = new Scanner(System.in);
            System.out.println("Date: ");
            String date = scan.next();
            Zodiac.DataResponse data=personStub.getData((Zodiac.DataRequest.newBuilder().setData(date).build()));
            System.out.println(data);
        }

        channel.shutdown();
    }

}
