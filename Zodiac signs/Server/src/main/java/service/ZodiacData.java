package service;

import io.grpc.stub.StreamObserver;
import proto.Zodiac;
import proto.PersonServiceGrpc;

public class ZodiacData extends PersonServiceGrpc.PersonServiceImplBase{
    @Override
    public void getData(Zodiac.DataRequest request, StreamObserver<Zodiac.DataResponse> responseObserver) {
        super.getData(request, responseObserver);
        Zodiac.DataResponse response=Zodiac.DataResponse.newBuilder().setZodiac((request.getData())).build();
        System.out.println("Zodiac: " + response.getZodiac());
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
